let proporcion = 60;
let velocidadMaximaCarril = 6;
let carriles = [];
let rana;
let victoria = false;
let cantidadCarriles;
let pixelesSeparacion = 5;

function setup(){
	frameRate(30);
	createCanvas(windowWidth, windowHeight);	
	cantidadCarriles = floor(windowHeight / (proporcion + pixelesSeparacion) );
	inicializarJuego();
}

function draw(){
	background(0);
	pintarCarriles();
	rana.pintar();
	if( verificarColision() ){
		crearRana();
	}
	verificarVictoria();
}

function inicializarJuego() {
	crearCarriles();
	crearRana();
	victoria = false;
}

function keyPressed() {
	if( keyCode == ENTER ){
		inicializarJuego();
		loop();
	}
	else{
		rana.saltar(keyCode);
	}
}

function pintarCarriles() {
	for (var i = 0; i < carriles.length; i++) {
		carriles[i].actualizarPosicion();
		carriles[i].pintar();
	}
}

function crearRana() {
	rana = new Rana( 
		windowWidth / 2 - proporcion / 2,
		cantidadCarriles * (proporcion + pixelesSeparacion),
		proporcion,
		windowWidth,
		windowHeight,
		pixelesSeparacion
	);
}

function crearCarriles(){
	let velocidadCarril = 0;
	carriles = [];
	for (var i = 1; i < cantidadCarriles ; i++) {
		velocidadCarril = floor( random( 5, velocidadMaximaCarril ) );
		carriles.push(
			new Carril( 
				0, 
				i * proporcion + (pixelesSeparacion * i),
				windowWidth,
				proporcion,
				proporcion * 2,
				velocidadCarril,
				(i % 2 == 0) ? RIGHT_ARROW : LEFT_ARROW
			)
		);
	}
}

function verificarColision() {
	for (var i = 0; i < carriles.length; i++) {
		if( carriles[i].verificarColision( rana ) ){
			return true;
		}
	}

	return false;
}

function verificarVictoria(){
	if( rana.y == 0 ){
		victoria = true;
		noLoop();
	}
}