class Auto extends Rectangulo {
	
	constructor( x, y, ancho, carril ) {
		super( x, y, ancho, carril.alto );
		this.carril = carril;
	}

	pintar() {
		fill("blue");
		rect( this.x, this.y, this.ancho, this.carril.alto );	
	}

	actualizarPosicion() {
		if( this.carril.direccion == LEFT_ARROW ){
			this.x -= this.carril.velocidad;
			if( this.x + this.ancho < 0 ){
				this.x = this.carril.ancho;
			}
		}
		else{
			this.x += this.carril.velocidad;
			if( this.x > this.carril.ancho ){
				this.x = 0 - this.ancho;
			}			
		}
	}	
}