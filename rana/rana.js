class Rana extends Rectangulo {

	constructor( x, y, proporcion, anchoPantalla, altoPantalla, pixelesSeparacion ) {
		super( x, y, proporcion, proporcion );
		this.proporcion = proporcion;
		this.anchoPantalla = anchoPantalla;
		this.altoPantalla = altoPantalla;
		this.pixelesSeparacion = pixelesSeparacion;
	}

	pintar() {
		fill("red");
		rect( this.x, this.y, this.proporcion, this.proporcion );	
	}

	saltar( direccion ) {
		switch( direccion ){
			case LEFT_ARROW:
				this.x -= this.proporcion + this.pixelesSeparacion;
			break;
			case RIGHT_ARROW:
				this.x += this.proporcion + this.pixelesSeparacion;
			break;
			case UP_ARROW:
				this.y -= this.proporcion + this.pixelesSeparacion;
			break;
			case DOWN_ARROW:
				this.y += this.proporcion + this.pixelesSeparacion;
			break;
		}
	}
}