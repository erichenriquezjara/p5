class Rectangulo {

	constructor( x, y, ancho, alto ) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
	}

	verificarColision( otro ){
		return collideRectRect(
			this.x,
			this.y,
			this.ancho,
			this.alto,
			otro.x,
			otro.y,
			otro.ancho,
			otro.alto
		);
	}
}