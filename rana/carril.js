class Carril extends Rectangulo {
	
	constructor( x, y, ancho, alto, proporcion, velocidad, direccion ) {
		super( x, y, ancho, alto );
		this.proporcion = proporcion;
		this.velocidad = velocidad;
		this.direccion = direccion;
		this.autos = [];
		this.crearAutos();
	}

	pintar() {
		fill("grey");
		rect( this.x, this.y, this.ancho, this.alto );

		for (var i = 0; i < this.autos.length; i++) {
			this.autos[i].pintar();
		}
	}

	verificarColision( otro ){
		for (var i = 0; i < this.autos.length; i++) {
			if( this.autos[i].verificarColision( otro ) ){
				return true;
			}
		}

		return false;
	}

	crearAutos(){
		let anchoAuto = floor( random(1, 3) ) * floor( random(this.proporcion, this.proporcion * 2) );
		let separacion = 0;
		let puntoX = floor( random(2) ) * this.proporcion;
		let largoTotal = puntoX;
		
		while( largoTotal < (this.ancho - this.proporcion) ){
			separacion = floor( random(2, 3) ) * this.proporcion;
			this.autos.push( 
				new Auto( 
					puntoX,
					this.y,
					anchoAuto,
					this
				)
			);
			largoTotal = largoTotal + anchoAuto + separacion;
			puntoX = largoTotal;
		}
	}

	actualizarPosicion() {
		for (var i = 0; i < this.autos.length; i++) {
			this.autos[i].actualizarPosicion();
		}
	}
}