class Tuberias {

	constructor( anchoPantalla, altoPantalla, proporcion ){
		this.anchoPantalla = anchoPantalla;
		this.altoPantalla = altoPantalla;
		this.proporcion = proporcion;

		do{
			this.tArriba = floor(random(altoPantalla / 2));
			this.tAbajo = floor(random(altoPantalla / 2));
		} 
		while( this.tArriba + this.tAbajo + proporcion * 3 > altoPantalla);

		this.x = anchoPantalla;
	}


	pintar(){
		fill("green");
		rect( this.x, 0, this.proporcion, this.tArriba);
		rect( this.x, this.altoPantalla - this.tAbajo, this.proporcion, this.tAbajo);
	}

	actualizarPosicion( vTubos ){
		this.x -= vTubos;
	}

	fueraDePantalla(){
		if( this.x + this.proporcion < 0 ){
			return true;
		}
		else{
			return false;
		}
	}

}