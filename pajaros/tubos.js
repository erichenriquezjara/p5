class Tubos {

	constructor( anchoPantalla, altoPantalla, proporcion ){
		this.anchoPantalla = anchoPantalla;
		this.altoPantalla = altoPantalla;
		this.anchoTubo = proporcion;

		do {			
			this.arriba = random( altoPantalla );
			this.abajo = random( altoPantalla );
		}
		while( this.arriba + this.abajo + proporcion * 4 > altoPantalla );

		this.x = anchoPantalla;
	}

	actualizarPosicion( velocidad ){
		this.x -= velocidad;
	}	
	
	pintar(){
		fill("green");
		rect( this.x, 0, this.anchoTubo, this.arriba);
		rect( this.x, this.altoPantalla - this.abajo, this.anchoTubo, this.abajo);
	}

	fueraDePantalla(){
		if( this.x + this.anchoTubo < 0 ){
			return true;
		}
		else{
			return false;
		}
	}

}