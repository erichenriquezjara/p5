class Pajaro {

	constructor( x, y, altoPantalla, proporcion, img ){
		this.x = x;
		this.y = y;
		this.altoPantalla = altoPantalla;
		this.proporcion = proporcion;
		this.aceleracion = 0;
		this.img = img;
	}

	pintar( contadorFrames ){
		//fill("red");
		//rect( this.x, this.y, this.proporcion, this.proporcion);

		
		let anchoImagen = 183;
		let altoImagen = 168;

		let cual = floor(contadorFrames / 2) % 14;
		let x = cual % 5;
		let y = floor(cual / 5) % 3;

		image(
			this.img,
			this.x, this.y,
			2 * this.proporcion, 2 * this.proporcion,
			x * anchoImagen, y * altoImagen,
			anchoImagen, altoImagen
		);
		
	}

	actualizarPosicion( gravedad ){
		this.aceleracion += gravedad;
		this.y += this.aceleracion;

		if( this.y > this.altoPantalla ){
			this.y = this.altoPantalla - this.proporcion;
			this.aceleracion = 0;
		}

		if( this.y < 0 ){
			this.y = 0;
			this.aceleracion = 0;
		}
	}

	volar( subida ){
		this.aceleracion += -subida;
	}

}