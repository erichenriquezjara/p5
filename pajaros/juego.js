let proporcion = 60;
let pajaro;
let gravedad = 1;
let subida = 13;

let tuberias = [];
let vTuberias = 10;

let perdimos = false;

let img;

function preload(){
	img = loadImage("./img/pajaros.png");
}

function setup(){
	frameRate(30);
	createCanvas(windowWidth, windowHeight);
	inicializarJuego();
}

function draw(){
	background(0);

	pajaro.pintar( frameCount );
	pajaro.actualizarPosicion( gravedad );

	agregarTuberia();
	pintarTuberias();
	elimiarTuberiasFueraDePantalla();
	verificarColision( pajaro, tuberias );

	if( perdimos ){
		perdimos = false;
		inicializarJuego();
	}
}

function inicializarJuego(){
	pajaro = new Pajaro( 50, windowHeight/2, windowHeight, proporcion, img);
	tuberias = [];
	perdimos = false;
}

function mouseClicked(){
	pajaro.volar(subida);
}

function agregarTuberia(){
	if( frameCount % 40 == 0 ){
		tuberias.push( new Tuberias(windowWidth, windowHeight, proporcion) );
	}
}

function pintarTuberias(){
	for (var i = 0; i < tuberias.length; i++) {
		tuberias[i].pintar();
		tuberias[i].actualizarPosicion( vTuberias );
	}
}

function verificarColision( p, t ){
	for (var i = 0; i < t.length; i++) {
		if( p.y < t[i].tArriba || p.y + proporcion > height - t[i].tAbajo ){
			if( p.x + proporcion > t[i].x ){
				perdimos = true;
				break;
			}
		}
	}
}

function elimiarTuberiasFueraDePantalla(){
	for (var i = 0; i < tuberias.length; i++) {
		if( tuberias[i].fueraDePantalla() ){
			tuberias.splice( i, 1 );
		}
	}
}