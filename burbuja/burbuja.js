var valores = [];
var ancho = 50;
var i = 0;
var j = 0;

function setup(){
	frameRate(60);
	createCanvas(windowWidth, windowHeight);
	for (var i = 0; i < width/ancho; i++) {
		valores[i] = random(height);
	}
}

function draw(){
	background(0);

	if( i < valores.length ){
		if ( j < valores.length - i - 1 ) {
			if( valores[j] > valores[j+1] ){
				cambiar( j, j+1 );
			}
			j++;
		}
		else{
			i++;
			j=0;
		}
	}
	else{
		noLoop();
	}

	pintarArreglo();
}

function pintarArreglo(){
	for (var n = 0; n < width/ancho; n++) {
		fill("#1423f7");
		rect( n * ancho, height - valores[n], ancho, valores[n] );
	}
}

function cambiar( j, k ){
	var buffer = valores[j];
	valores[j] = valores[k];
	valores[k] = buffer;
}