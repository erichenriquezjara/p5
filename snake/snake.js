let serpiente;
let fruta;
let direccion;
let escala = 60;

let cantidadX;
let cantidadY;

let perdimos;
let comimosFruta;

let velocidad;

function setup(){
	frameRate(3);
	createCanvas(windowWidth, windowHeight);
	
	cantidadX = floor(windowWidth / escala);
	cantidadY = floor(windowHeight / escala);

	direccion = LEFT_ARROW;
	inicializarJuego();
}

function draw(){
	background(0);
	frameRate(velocidad / 2);
	dibujarSerpiente();
	moverSerpiente();

	if( perdimos ){
		perdimos = false;
		inicializarJuego();
	}
}

function dibujarSerpiente(){
	fill("green");
	for (var i = 0; i < serpiente.length; i++) {
		rect( serpiente[i].x, serpiente[i].y, escala, escala );
	}

	fill("red");
	rect( fruta.x, fruta.y, escala, escala );
}

function moverSerpiente(){
	nuevaCabeza = createVector( serpiente[0].x, serpiente[0].y );
	switch( direccion ){
		case LEFT_ARROW:
			nuevaCabeza.x = nuevaCabeza.x - escala;
		break;
		case RIGHT_ARROW:
			nuevaCabeza.x = nuevaCabeza.x + escala;
		break;
		case UP_ARROW:
			nuevaCabeza.y = nuevaCabeza.y - escala;
		break;
		case DOWN_ARROW:
			nuevaCabeza.y = nuevaCabeza.y + escala;
		break;
	}

	serpiente.unshift(nuevaCabeza);

	if(comimosFruta){
		comimosFruta = false;
	}
	else{
		serpiente.pop();
	}

	verificarColision();
}

function inicializarJuego(){
	crearFruta();
	comimosFruta = true;
	perdimos = false;
	velocidad = 10;
	serpiente = [
		createVector( escala * floor(random(cantidadX)), escala * floor(random(cantidadY)) )
	];
}

function keyPressed(){
	direccion = keyCode;
}

function verificarColision(){
	// Fuera del tablero
	if( serpiente[0].x < 0 || serpiente[0].x > windowWidth){
		perdimos = true;
		return;
	}
	if( serpiente[0].y < 0 || serpiente[0].y > windowHeight){
		perdimos = true;
		return;
	}

	// Si chocamos con nuestro cuerpo
	let chocamos = false;
	for (var i = 1; i < serpiente.length; i++) {
		if( serpiente[i].x == serpiente[0].x && serpiente[i].y == serpiente[0].y ){
			chocamos = true;
			break;
		}
	}
	if(chocamos){
		perdimos = true;
		return;
	}

	// Verificamos si comimos la fruta
	if( serpiente[0].x == fruta.x && serpiente[0].y == fruta.y ){
		comerFruta();
	}
}

function crearFruta(){
	fruta = createVector( escala * floor(random(cantidadX)), escala * floor(random(cantidadY)) )
}

function comerFruta(){
	comimosFruta = true;
	velocidad++;
	crearFruta();
}