let tablero = [];
let jugadores = ['X','O'];
let juegoTerminado = false;
let jugadorActual;
let espaciosDisponibles = 9;

function setup(){
	frameRate(60);
	createCanvas(windowWidth, windowHeight);
/*
	tablero = [
		['X','O','X'],
		['0','0','O'],
		['X','X','X']
	];
	*/

	reiniciarTablero();
}

function mousePressed(){
	let ejeX = mouseX / (width / 3);
	let ejeY = mouseY / (height / 3);
	console.log( floor(ejeX), floor(ejeY) );
	turno( floor(ejeX), floor(ejeY) );
}

function turno( x, y ){
	if( tablero[x][y] != '' ){
		console.log('Ocupado');
	}
	else{
		espaciosDisponibles--;
		tablero[x][y] = jugadores[jugadorActual];
		jugadorActual = ( jugadorActual + 1 ) % 2;
	}
}

function reiniciarTablero(){
	tablero = [
		['','',''],
		['','',''],
		['','','']
	];

	juegoTerminado = false;
	jugadorActual = floor(random( jugadores.length ));
	espaciosDisponibles = 9;
}

function keyPressed(){
	if( keyCode == ENTER ){
		reiniciarTablero();
	}
}

function draw(){
	background(255);
	dibujarTablero();

	if( !juegoTerminado ){
		let mensaje = comprobarGanador();

		if( mensaje != null){
			setTimeout(
				function(){
					alert( mensaje );
				}
			,1)
		}
	}
}

function iguales( a, b, c){
	return a == b && b == c && a != '';
}

function comprobarGanador(){
	let ganador = null;

	// Horizontales
	for( let i=0 ; i < 3; i++ ){
		if ( iguales( tablero[i][0], tablero[i][1], tablero[i][2] ) ) {
			ganador = tablero[i][0];
			juegoTerminado = true;
		}
	}

	// Verticales
	for( let i=0 ; i < 3; i++ ){
		if ( iguales( tablero[0][i], tablero[1][i], tablero[2][i] ) ) {
			ganador = tablero[0][i];
			juegoTerminado = true;
		}
	}

	// Diagonales
	if( iguales( tablero[0][0], tablero[1][1], tablero[2][2] ) ){
		ganador = tablero[0][0];
		juegoTerminado = true;
	}
	if( iguales( tablero[2][0], tablero[1][1], tablero[0][2] ) ){
		ganador = tablero[2][0];
		juegoTerminado = true;
	}

	if( ganador != null ){
		return 'El ganador es: ' + ganador;
	}

	if( ganador == null && espaciosDisponibles == 0) {
		juegoTerminado = true;
		return 'Empate'
	}
}

function dibujarTablero(){
	let w = width / 3;
	let h = height / 3;
	stroke("blue");
	strokeWeight(3);

	// Verticales
	line( w, 0, w, h*3 );
	line( w*2, 0, w*2, h*3 );

	// Horizontales
	line( 0, h, width, h );
	line( 0, h*2, width, h*2 );

	for( let j=0 ; j < 3; j++ ){
		for( let i=0 ; i < 3; i++ ){
			let x = w * i + w / 2;
			let y = h * j + h / 2;

			let r = w / 4;

			let simbolo = tablero[i][j];

			noFill();
			if( simbolo == 'X' ){
				stroke("red");
				line( x - r, y - r, x + r, y + r );
				line( x + r, y - r, x - r, y + r );
			}
			else {
				if( simbolo == 'O' ){
					stroke("green");
					ellipse( x, y, r*2);
				}
			}
		}
	}
}